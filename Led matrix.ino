#define ROW1 5
#define ROW2 6
#define COL1 3
#define COL2 4

int actions[4][4] = {
  {0, 1, 1, 0},
  {1, 0, 1, 0},
  {0, 1, 0, 1},
  {1, 0, 0, 1}
};


void setup() {
  Serial.begin(9600);
  pinMode(ROW1, OUTPUT);
  pinMode(ROW2, OUTPUT);
  pinMode(COL1, OUTPUT);
  pinMode(COL2, OUTPUT);
}


void do_blink(int k){
  for (int i = 0; i < 4; i++){
    digitalWrite(i + 3 , actions[k][i]);
  }
}

char todo[4] = {-1, -1, -1, -1};


void loop() {
  // if (Serial.available()){
  //   int step = 0;
  //   while (Serial.available()) {
  //     char s = Serial.read();
  //     todo[step] = s;
  //     //Serial.print(todo[step]);
  //     step++;
  //     delay(100);
  //   }
  //   while (step < 4) {
  //     todo[step] = -1;
  //     step++;
  //   }
  //   for (int i = 0; i < 4; i++){
  //     Serial.print(char(todo[i]));
  //   }
  //   Serial.println();
  // }
  for (int i = 0; i < 4; i++){
    if (todo[i] != -1){
      do_blink(todo[i - '0']);
      delay(1000);
    }
  }
}
